Top-level API
=============

.. automodule:: marshmallow
   :members:
   :autosummary:
   :exclude-members: OPTIONS_CLASS

.. data:: EXCLUDE
.. data:: INCLUDE
.. data:: RAISE
.. data:: missing
